# README #

This README documents whatever steps are necessary to get this application up and running.

### About this application ###

This snakemake application is a derivation of the Fast-GBS pipeline. 
The Fast-GBS pipeline uses a wide range of tools to achieve it's goal, which is
the Genotyping-by-sequencing. This project took over some of the tools and made them to
a snakemake format. Also it added the quality measurement. 

### How do I get set up? ###

To make use of this application the following software is needed:
	* Snakemake
	* BWA
	* Samtools
	* Python 3 (or higher)
	* Matplotlib (python package)
	* pysam	(python package)

To start the program snakemake has to be installed and a virtual environment has te be set up
The environment can be created with this command:
	- "virtualenv -p /usr/bin/python3 venv"

To start the environment this command needs to be filled in the commandline:
	- "source venv/bin/activate"

For the packages and Snakemake a few pip installs need to be done.
pip installs:
	- "pip3 install snakemake"
	- "pip3 install matplotlib"
	- "pip3 install pysam"

For the python and snakemake additions the install instructions need to be followed.
	- BWA: http://bio-bwa.sourceforge.net/
	- Samtools: http://www.htslib.org/
	- Python: https://www.python.org/

HOW TO RUN:

First change the "config.yaml" to the wanted parameters and working directory.
After the changes run the command:
	- "snakemake"

### Contribution ###

Contributers:
	- Fenna Feenstra (Lecturer)
	- Creators of Fast-GBS pipeline


### Who do I talk to? ###

Author: Menno Gerbens
Contact: m.j.gerbens@st.hanze.nl
